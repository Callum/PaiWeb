from django.db import models
import datetime
import uuid

# Create your models here.

class PaiJob(models.Model):
    JobId = models.UUIDField(primary_key=False, default=uuid.uuid4, editable=False)
    JobName = models.CharField(max_length=200, default="PAI Search")
    SearchDate = models.DateField(default=datetime.date.today, editable=False)
    ChartSeries = models.TextField(default="")
    PngString = models.TextField(default="")
    PaiGenes = models.TextField(default="")
    def __str__(self):
        return str(self.JobId)

