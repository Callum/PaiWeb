# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-26 17:01
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('PAI_WEB', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='paijob',
            name='ChartSeries',
            field=models.TextField(default=''),
        ),
    ]
