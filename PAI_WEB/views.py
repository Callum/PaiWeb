from django.shortcuts import render, get_object_or_404, redirect
from django.http import Http404, HttpResponse
from .models import PaiJob
from .forms import PaiForm, DlForm

import sys
sys.path.append('/home/callum/Documents/Projects')
import pai.paifinder as pf
import pai.plot as plot
import pai.geneFinder as gf

from json import dumps, loads
from subprocess import call
import base64
import os
import csv

# Create your views here.


def index(request):
    joblist = PaiJob.objects.order_by('-id')[:5]
    form = PaiForm(request.POST, request.FILES)
    error = False
    context = {'joblist': joblist, 'form': form,
               'errors': []}

    if form.is_valid():
        fastainfo = pf.get_seq(request.FILES['genome_file'])
        if type(fastainfo) is list:
            sequence = fastainfo[0]
        else:
            context['errors'].append(fastainfo)
            error = True

        if len(request.FILES) > 1:
            genepos = pf.get_pos(request.FILES['gff_file'])
        elif not error:
            genepos = gf.mainloop(sequence, 30, 0.98, 3)

        if error:
            return render(request, 'index.html', context)

        jobname = form.cleaned_data['job_name']
        if not jobname:
            if len(fastainfo[1]) > 0:
                jobname = fastainfo[1][:50]
            else:
                jobname = 'PAI search'

        seqlen = len(sequence)
        posinfo = pf.predictpai(sequence, genepos, 10000, 2000, 2)
        codons_merged = pf.merge_adjacent(posinfo[2])
        kmer_merged = pf.merge_adjacent(posinfo[3])
        pred_merged = pf.merge_adjacent(posinfo[5])

        pai_genes = pf.find_pai_genes(genepos, pred_merged, 2000)
        pred_final = plot.parseprecise(pai_genes)
        chartdata = pf.make_chartdata(seqlen, jobname, posinfo[0], posinfo[1],
                                   codons_merged, kmer_merged, posinfo[4],
                                   pred_final)

        n = PaiJob(JobName=jobname, ChartSeries=chartdata, PaiGenes=dumps(pai_genes))
        uuid = str(n.JobId)
        bchart = bytes('{' + chartdata + '};', 'utf-8')
        with open('tmp/' + uuid + '.json', 'wb+') as dest:
            dest.write(bchart)

        infile = 'tmp/' + uuid + '.json'
        outfile = 'tmp/' + uuid + '.png'
        call(['phantomjs', 'static/highchart/highcharts-convert.js',
                           '-infile', infile, '-outfile', outfile])
        with open(outfile, 'rb') as pngfile:
            pngstring = base64.b64encode(pngfile.read())
        os.remove(infile)
        os.remove(outfile)
        n.PngString = pngstring
        n.save()
        return redirect('results', jobuuid=uuid)
    return render(request, 'index.html', context)


def results(request, jobuuid):
    try:
        job = get_object_or_404(PaiJob, JobId=jobuuid)
    except ValueError:
        raise Http404("Job does not exist")

    chartdata = job.ChartSeries
    pngstring = job.PngString
    pai_genes = loads(job.PaiGenes)
    jobname = job.JobName

    form = DlForm(request.POST)
    if form.is_valid():
        outfmt = form.cleaned_data['dl_type']
        if outfmt == 'SC' :
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = \
                'attachment; filename="{}_table.csv"'.format(jobname)
            csvdata = pf.output_formats(pai_genes, 'sum')
            writer = csv.writer(response)
            for row in csvdata:
                writer.writerow(row)
        elif outfmt == 'ST':
            response = HttpResponse(content_type='text/tsv')
            response['Content-Disposition'] = \
                'attachment; filename="{}_table.tsv"'.format(jobname)
            csvdata = pf.output_formats(pai_genes, 'sum')
            writer = csv.writer(response, delimiter='\t')
            for row in csvdata:
                writer.writerow(row)
        return response

    context = {'jobid': str(job), 'chart': chartdata, 'form': form,
               'pngstring': pngstring, 'paigenes': pai_genes}
    return render(request, 'results.html', context)
