from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<jobuuid>.+)/chart$', views.results, name="chart"),
    url(r'^(?P<jobuuid>.+)$', views.results, name="results"),
]