from django import forms


class PaiForm(forms.Form):
    job_name = forms.CharField(max_length=200, label='Job Name',
                               required=False)
    genome_file = forms.FileField(label='Genome fasta file')
    gff_file = forms.FileField(label='Optional gff3 gene file',
                               required=False)

class DlForm(forms.Form):
    DOWNLOAD_CHOICES = (
        ('SC', 'Summary (csv)'),
        ('ST', 'Summary (tsv)')
    )
    dl_type = forms.ChoiceField(choices=DOWNLOAD_CHOICES, label="Download type")
